package net.intmario.distributedapps;

import java.io.IOException;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Scanner;

public class App {
    static void help() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Available commands:\n");

        System.out.println(stringBuilder.toString());
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s;

        Peer peer = new Peer();
        peer.setAddresses(new DNSPeerFinder().findPeers());

        Thread t = new Thread(peer, "peer");
        t.start();

        System.out.println("Welcome to BitLab! Type 'q' to quit.\n");

        //Loop until user decided to exit (typed "q")
        do {
            //Get user's input
            System.out.print("BitLab > ");

            s = scanner.nextLine();
            //And split it into separate words
            String[] strings = s.split(" ");

            //If input is not empty
            if (strings.length > 0) switch (strings[0]) {
                    /*
                        DNS Lookup - looks for peers IP by DNS list from *.txt file
                     */
                case "dns":
                    //Look for new IP's
                    ArrayList<String> peers = new DNSPeerFinder().findPeers();
                    System.out.println("Found peers - " + peers.size() + "\n");
                    for (String str : peers)
                        System.out.println("\t" + str);
                    System.out.println();

                    //Update addresses in peer
                    peer.setAddresses(new DNSPeerFinder().findPeers());
                    break;

                    /*
                        Sends addresses to peers.
                     */
                case "addr":
                    System.out.println("Sent addresses.");
                    peer.sendMessage("addr");
                    break;

                    /*
                        Receives addresses from peers.
                     */
                case "getaddr":
                    System.out.println("Asked for addresses.");
                    peer.sendMessage("getaddr");
                    break;

                    /*
                        Connects to random peer. It doesn't sent version message!
                     */
                case "connect":
                    System.out.println("Trying to connect...");
                    SocketChannel channel = peer.connectToRandom();

                    if (channel != null) {
                        try {
                            System.out.println("\n\tConnected to: " + channel.getRemoteAddress().toString() + "\n");
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                    }
                    else {
                        System.out.println("\n\tUnable to connect!\n\n");
                    }
                    break;


                    /*
                        Version message
                     */
                case "version":
                    System.out.println("Sent version message.");
                    peer.sendMessage("version");
                    break;

                    /*
                        Verack - response for version
                     */
                case "verack":
                    System.out.println("Sent verack message.");
                    peer.sendMessage("verack");
                    break;

                    /*
                        ping - are you there?
                     */
                case "ping":
                    System.out.println("Sent ping request.");
                    peer.sendMessage("ping");
                    break;


                    /*
                        Test reject message. Sends "test" in it's body.
                     */
                case "reject":
                    System.out.println("Sent reject command.");
                    peer.sendMessage("reject");
                    break;

                    /*
                        Gets data by sending inventory vectors.
                     */
                case "getdata":
                    System.out.println("Sent getdata command.");
                    peer.sendMessage("getdata");
                    break;

                    /*
                        Sends info about blocks/transactions.
                     */
                case "inv":
                    System.out.println("Sent info about blocks/transactions");
                    peer.sendMessage("inv");
                    break;

                    /*
                        Sends headers to peer.
                     */
                case "headers":
                    System.out.println("Sent headers.");
                    peer.sendMessage("headers");
                    break;

                    /*
                        Receives headers from peer.
                     */
                case "getheaders":
                    System.out.println("Asked for headers.");
                    peer.sendMessage("getheaders");
                    break;

                    /*
                        Receives blocks from peer.
                     */
                case "getblocks":
                    System.out.println("Asked for blocks.");
                    peer.sendMessage("getblocks");
                    break;

                    /*
                        Prints info about peers' connections
                     */
                case "info":
                    peer.info();
                    break;
            }
        }
        //Exiting
        while (!s.startsWith("q"));

        peer.stop();

        System.out.println("Thank you for using BitLab.\nExiting...\n");
    }
}
