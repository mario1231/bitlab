package net.intmario.distributedapps;

import java.io.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class DNSPeerFinder {
    ArrayList<String> dnsServers = new ArrayList<>();
    ArrayList<String> peers = new ArrayList<>();
    String filepath = "";

    public DNSPeerFinder() {
        this.filepath = "dns.txt";

        try {
            dnsServers.addAll(Files.readAllLines(Paths.get(this.filepath)));
        }
        catch (IOException ex) {
            System.out.println("ERROR reading DNS file.");
        }
    }

    public DNSPeerFinder(String filepath) {
        this.filepath = filepath;

        try {
            dnsServers.addAll(Files.readAllLines(Paths.get(this.filepath)));
        }
        catch (IOException ex) {
            System.out.println("ERROR reading DNS file.");
        }
    }

    public ArrayList<String> findPeers() {
        peers.clear();

        for (String dnsServer :dnsServers) {
            try {
                InetAddress dnses[] = InetAddress.getAllByName(dnsServer);

                for (InetAddress address : dnses) {
                    peers.add(address.getHostAddress());
                }
            } catch (UnknownHostException ex) {
                ex.printStackTrace();
            }
        }

        return this.peers;
    }

    ArrayList<String> getPeers() { return this.peers; }
}
