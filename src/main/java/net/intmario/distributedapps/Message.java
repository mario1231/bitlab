package net.intmario.distributedapps;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

/*
    @class Message

    This class represents the message sent to peer.
    It combines header and payload and gives it back to buffer.
 */
public class Message {
    public static byte[] randomNonce() {
        return ByteBuffer.allocate(8).putLong(new Random().nextLong()).array();
    }

    public static int InvIntLength(byte b) {
        return (b & 0xff) < 0xfd ? 1 : (b & 0xff) == 0xfd ? 3 : (b & 0xff) == 0xfe ? 5 : 9;
    }

    public static int InvInt(byte[] bytes) {
        int length = Message.InvIntLength(bytes[0]);

        if (length == 1)
            return bytes[0] & 0xff;
        else if (length == 3)
            return 0x100 * (bytes[2] & 0xff) + (bytes[1] & 0xff);
        else if (length == 5)
            return 0x10000 * (bytes[3] & 0xff) + 0x100 * (bytes[2] & 0xff) + (bytes[1] & 0xff);
        else if (length == 9)
            return 0x1000000 * (bytes[4] & 0xff) + 0x10000 * (bytes[3] & 0xff) + 0x100 * (bytes[2] & 0xff) + (bytes[1] & 0xff);
        else return 0;
    }

    public static byte[] GenerateInvInt(long i) {
        i = Math.abs(i);

        if (i < 0xfd)
            return new byte[] {(byte)i};
        else if (i <= 0xffff)
            return ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN)
                .put((byte)0xfd)
                .putShort((short)i).array();
        else if (i <= 0xffffffff)
            return ByteBuffer.allocate(5).order(ByteOrder.LITTLE_ENDIAN).
                    put((byte)0xfe).putInt((int)i).array();
        else
            return ByteBuffer.allocate(9).order(ByteOrder.LITTLE_ENDIAN)
            .put((byte)0xff).putLong(i).array();
    }

    public static final byte[] MAGIC = { (byte)0xf9, (byte)0xbe, (byte)0xb4, (byte)0xd9 };
    MessageHeader header;
    byte[] payload;

    public Message(String command) {
        this.payload = new byte[0];
        header = new MessageHeader(command, payload);
    }

    public Message(String command, byte[] payload) {
        this.payload = payload;
        header = new MessageHeader(command, payload);
    }

    public ByteBuffer getMessage() {
        return ByteBuffer.allocate(header.getHeader().length + payload.length)
                .put(this.header.getHeader())
                .put(payload);
    }
}

/*
    The header of the message.
 */
class MessageHeader {
    private byte[] payload;
    private byte[] command;
    private byte[] payloadLength;
    private byte[] checksum;

    public MessageHeader(String command, byte[] payload) {
        this.payload = payload;
        this.payloadLength = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(payload.length).array();
        this.command = ByteBuffer.allocate(12).put(command.getBytes()).array();
        this.checksum = calculateChecksum();
    }

    public byte[] calculateChecksum() {
        byte[] checksum = null;

        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            checksum = messageDigest.digest(messageDigest.digest(payload));
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        }

        return Arrays.copyOfRange(checksum, 0, 4);
    }

    public byte[] getHeader() {
        return ByteBuffer.allocate(24)
                .put(Message.MAGIC)
                .put(command)
                .put(payloadLength)
                .put(checksum)
                .array();
    }
}

/*
    This is payload of "addr" message.
 */
class AddrPayload {
    private ArrayList<AddressStruct> list;

    public AddrPayload() {
        list = MessageParser.addresses;
    }

    public byte[] getPayload() {
        byte[] length = Message.GenerateInvInt(list.size());
        ByteBuffer byteBuffer = ByteBuffer.allocate(30 * list.size() + length.length);
        byteBuffer.put(length);

        for (AddressStruct s : list)
            byteBuffer.put(s.timestamp)
                    .put(s.services)
                    .put(s.address)
                    .put(s.port);

        return byteBuffer.array();
    }
}

/*
    This is payload of "headers" message.
 */
class HeadersPayload {
    ArrayList<HeaderStruct> headers;

    public HeadersPayload() {
        this.headers = MessageParser.headers;
    }

    public byte[] getPayload() {
        byte[] length = Message.GenerateInvInt(headers.size());

        ByteBuffer byteBuffer =  ByteBuffer.allocate(headers.size() * 81 + length.length)
                .put(length);

        for (HeaderStruct s : headers)
            byteBuffer.put(s.version).put(s.prevBlock)
            .put(s.merkleRoot).put(s.timestamp).put(s.bits)
            .put(s.nonce).put(s.txnCount);

        return byteBuffer.array();
    }
}

/*
    This is payload of "inv" message.
 */
class InvPayload {
    ArrayList<InventoryVectorStruct> inventoryVectors;

    public InvPayload() {
        this.inventoryVectors = MessageParser.inventoryVectors;
    }

    public byte[] getPayload() {
        byte[] length = Message.GenerateInvInt(inventoryVectors.size());

        ByteBuffer byteBuffer =  ByteBuffer.allocate(inventoryVectors.size() * 36 + length.length)
                .put(length);

        for (InventoryVectorStruct s : inventoryVectors)
            byteBuffer.put(s.type).put(s.hash);

        return byteBuffer.array();
    }
}

/*
    This is payload of "getheaders" message.
 */
class GetHeadersPayload {
    public byte[] getPayload() {
        return ByteBuffer.allocate(69)
                .putInt(1)
                .put((byte)1)
                .putInt(0)
                .putInt(0)
                .array();
    }
}

/*
    This is payload of "getblocks" message.
 */
class GetBlocksPayload {
    public byte[] getPayload() {
        return ByteBuffer.allocate(69)
                .putInt(1)
                .put((byte)1)
                .putInt(0)
                .putInt(0)
                .array();
    }
}

/*
    This is payload of "reject" message.
 */
class RejectPayload {
    private String message;
    private char ccode;
    private String reason;
    private char[] data;

    public static char REJECT_MALFORMED = 0x01;
    public static char REJECT_INVALID = 0x10;
    public static char REJECT_OBSOLETE = 0x11;
    public static char REJECT_DUPLICATE = 0x12;
    public static char REJECT_NONSTANDARD = 0x40;
    public static char REJECT_DUST  = 0x41;
    public static char REJECT_INSUFFICIENTFEE = 0x42;
    public static char REJECT_CHECKPOINT = 0x43;

    public RejectPayload(String message, char ccode, String reason) {
        this.message = message;
        this.ccode = ccode;
        this.reason = reason;
    }

    public byte[] getPayload() {
        return ByteBuffer.allocate(1 + message.length() + 1 + 1 + reason.length())
                .put((byte)message.length())
                .put(message.getBytes())
                .put((byte)ccode)
                .put((byte)reason.length())
                .put((reason.getBytes()))
                .array();
    }
}

/*
    This is payload of "ping" message.
 */
class PingPayload {
    public byte[] getPayload() {
        return Message.randomNonce();
    }
}

/*
    This is payload of "pong" message.
 */
class PongPayload {
    private byte[] nonce;

    public PongPayload(byte[] nonce) {
        this.nonce = nonce;
    }

    public byte[] getPayload() {
        return nonce;
    }
}

/*
    This is payload of "version" message.
 */
class VersionPayload {
    private Address receiverAddress;
    private Address senderAddress;
    private byte[] nonce;

    public VersionPayload(String receiverAddress) {
        setReceiverAddress(receiverAddress);
        nonce = Message.randomNonce();
    }

    public byte[] getVersion() {
        return ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(70015).array();
    }

    public byte[] getServices() {
        return ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN).putLong(0x01).array();
    }

    public byte[] getTimestamp() {
        return ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN).putLong(System.currentTimeMillis() / 1000).array();
    }

    public byte[] getReceiverAddress() {
        return this.receiverAddress.getAddressWithoutTimestamp();
    }

    public byte[] getSenderAddress() {
        return this.senderAddress.getZerosWithoutTimestamp();
    }

    public byte[] getUserAgent() {
        return ByteBuffer.allocate(1).put((byte)0x00).array();
    }

    public byte[] getStartHeight() {
        return ByteBuffer.allocate(4).putInt(0).array();
    }

    public byte[] getRelay() {
        return ByteBuffer.allocate(1).put((byte)0).array();
    }

    public byte[] getPayload() {
        return ByteBuffer.allocate(4 + 16 + 26 + 26 + 8 + 1 + 4 + 1)
                .put(getVersion())
                .put(getServices())
                .put(getTimestamp())
                .put(getReceiverAddress())
                .put(getSenderAddress())
                .put(nonce)
                .put(getUserAgent())
                .put(getStartHeight())
                .put(getRelay())
                .array();
    }

    public void setReceiverAddress(String address) {
        String[] receiver = address.split(":");

        if (receiver[0].startsWith("/"))
            receiver[0] = receiver[0].substring(1);

        int receiverPort = Integer.parseInt(receiver[1]);

        this.receiverAddress = new Address(receiver[0], receiverPort);
        this.senderAddress = new Address(true);
    }
}

/*
    This is object representing IP address.
 */
class Address {
    private String address;
    private int port;
    private boolean isNull = false;

    public Address(String address, int port) {
        this.address = address;
        this.port = port;
    }

    public Address(boolean isNull) {
        this.isNull = true;
    }

    public byte[] getTimestamp() {
        return ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt((int)(System.currentTimeMillis() / 1000)).array();
    }

    public byte[] getServices() {
        if (!isNull)
            return ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN).putLong(0x00).array();
        else
            return ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN).putLong(0x01).array();
    }

    public byte[] getIp() {
        if (isNull) {
            return ByteBuffer.allocate(16).array();
        } else {

            String[] split = address.contains(".") ? address.split("\\.") : address.split(":");

            int[] splitInt = {
                    Integer.parseInt(split[0]),
                    Integer.parseInt(split[1]),
                    Integer.parseInt(split[2]),
                    Integer.parseInt(split[3]),
            };

            byte[] add = ByteBuffer.allocate(16).array();

            add[10] = (byte) 0xff;
            add[11] = (byte) 0xff;
            add[12] = (byte) splitInt[0];
            add[13] = (byte) splitInt[1];
            add[14] = (byte) splitInt[2];
            add[15] = (byte) splitInt[3];

            return add;
        }
    }

    public byte[] getPort() {
        return ByteBuffer.allocate(2).putShort((short)this.port).array();
    }

    public byte[] getAddress() {
        return ByteBuffer.allocate(30).order(ByteOrder.LITTLE_ENDIAN)
                .put(getTimestamp())
                .put(getServices())
                .put(getIp())
                .put(getPort()).array();
    }

    public byte[] getAddressWithoutTimestamp() {
        return ByteBuffer.allocate(26)
                .put(getServices())
                .put(getIp())
                .put(getPort()).array();
    }

    public byte[] getZerosWithoutTimestamp() {
        return ByteBuffer.allocate(26)
                .put(getServices())
                .array();
    }
}