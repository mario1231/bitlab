package net.intmario.distributedapps;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Random;

/*
    @class Peer

    This class represents peer in BitCoin network. It allows peer to create
    multiple connections and send messages to one or more of it's peers.
 */
public class Peer implements Runnable {
    private ArrayList<String> connectedPeers = new ArrayList<>();
    private ArrayList<String> availablePeers = new ArrayList<>();
    private ArrayList<String> addresses = new ArrayList<>();
    private ArrayList<SocketChannel> socketChannels = new ArrayList<>();

    private boolean onExit = false;

    /*
        The version messages list.
        Every peer requires it's oun version message
        Once version it's sent you can start communication.
     */
    private ArrayList<Message> versionMessages = new ArrayList<>();

    //Checkers
    public boolean isConnected() {
        if (socketChannels.size() == 0)
            return false;

        for (SocketChannel socketChannel : socketChannels) {
            if (!socketChannel.isConnected())
                return false;
        }

        return true;
    }

    /*
        This method allows to connect to random of available peers from DNS lookup.
        IT TRIES to connect, but not every connection is successful.
        It doesn't send any welcome messages, just establishes TCP connection.
     */
    public SocketChannel connectToRandom() {
        SocketChannel connected = null;

        if (this.availablePeers.size() > 0) {
            int maxTries = 100;

            do {
                maxTries--;
                int index = new Random().nextInt(this.availablePeers.size());

                try {
                    InetSocketAddress hostAddress = new InetSocketAddress(availablePeers.get(index), 8333);
                    SocketChannel channel = SocketChannel.open(hostAddress);
                    channel.configureBlocking(false);

                    connectedPeers.add(availablePeers.get(index));
                    availablePeers.remove(index);
                    socketChannels.add(channel);
                    connected = channel;
                    maxTries = 0;
                } catch (IOException ex) {
                }
            } while (maxTries > 0);
        } else {
            System.out.println("No free peers found!");
        }

        return connected;
    }

    /*
        Thread waiting for incoming packets.
     */
    @Override
    public void run() {
        while (!onExit) {
            try {
                for (SocketChannel channel : socketChannels) {
                    ByteBuffer byteBuffer = ByteBuffer.allocate(1000000);
                    int read = 0;
                    int r;
                    while ((r = channel.read(byteBuffer)) > 0)
                        read += r;

                    if (read > 0)
                        new MessageParser(byteBuffer.array()).parse();
                }
                Thread.sleep(1000);
            } catch (InterruptedException ex) {} catch (IOException ex) {}
        }
    }

    public void sendMessage(String messageType) {
        if (!isConnected()) {
            System.out.println("ALERT: You need to connect to minimum one of peers!");
            return;
        }

        if (messageType.equals("version")) {
            versionMessages = new ArrayList<>();

            for (SocketChannel socketChannel : socketChannels) {
                try {
                    versionMessages.add(new Message("version",
                            new VersionPayload(
                                socketChannel.getRemoteAddress().toString()
                            ).getPayload()));
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }

        for (int i = 0; i < socketChannels.size(); ++i) {
            try {
                ByteBuffer byteBuffer;
                switch (messageType) {
                    case "version":
                        byteBuffer = versionMessages.get(i).getMessage();
                        break;

                    case "verack":
                        byteBuffer = new Message("verack").getMessage();
                        break;

                    case "ping":
                        byteBuffer = new Message("ping", new PingPayload().getPayload()).getMessage();
                        break;

                    case "getaddr":
                        byteBuffer = new Message("getaddr").getMessage();
                        break;

                    case "addr":
                        byteBuffer = new Message("addr", new AddrPayload().getPayload()).getMessage();
                        break;

                    case "reject":
                        byteBuffer = new Message("reject", new RejectPayload("test", RejectPayload.REJECT_CHECKPOINT, "test").getPayload()).getMessage();
                        break;

                    case "inv":
                        byteBuffer = new Message("inv", new InvPayload().getPayload()).getMessage();
                        break;

                    case "getdata":
                        byteBuffer = new Message("getdata", new HeadersPayload().getPayload()).getMessage();
                        break;

                    case "headers":
                        byteBuffer = new Message("headers", new HeadersPayload().getPayload()).getMessage();
                        break;

                    case "getheaders":
                        byteBuffer = new Message("getheaders", new GetHeadersPayload().getPayload()).getMessage();
                        break;

                    case "getblocks":
                        byteBuffer = new Message("getblocks", new GetBlocksPayload().getPayload()).getMessage();
                        break;

                        default:
                            System.out.println("Unrecognized command.");
                            return;
                }

                byteBuffer.flip();

                if (socketChannels.get(i).isConnected()) {
                    while (byteBuffer.hasRemaining())
                        socketChannels.get(i).write(byteBuffer);
                }
            } catch (IOException ex) {

            }
        }
    }

    /*
        Closes all connections with peers.
     */
    public void stop() {
        this.onExit = true;

        try {
            for (SocketChannel socketChannel : socketChannels)
                socketChannel.close();
        } catch (IOException ex ) {
            System.out.println("Couldn't close channel!");
        }
    }

    public void setAddresses(ArrayList<String> addresses) {
        this.addresses = addresses;
        this.availablePeers = addresses;
    }

    /*
        Prints info about all of peer's connections.
     */
    public void info() {
        for (String s : connectedPeers)
            System.out.println("Connected to: " + s + ":8333");
    }
}

