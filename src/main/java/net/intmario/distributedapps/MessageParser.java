package net.intmario.distributedapps;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;

/*
    This object parses any incoming message from peer.
 */
public class MessageParser {
    byte[] message;
    ArrayList<byte[]> messages;

    //Global objects to access by invoked messages.
    public static ArrayList<HeaderStruct> headers = new ArrayList<>();
    public static ArrayList<AddressStruct> addresses = new ArrayList<>();
    public static ArrayList<InventoryVectorStruct> inventoryVectors = new ArrayList<>();

    public MessageParser(byte[] message) {
        this.message = message;
    }

    private void splitMessages() {
        messages = new ArrayList<>();
        ArrayList<Integer> indexes = new ArrayList<>();

        for (int i = 0; i < message.length - 3; ++i) {
            if (
                    message[i] == Message.MAGIC[0] &&
                    message[i+1] == Message.MAGIC[1] &&
                    message[i+2] == Message.MAGIC[2] &&
                    message[i+3] == Message.MAGIC[3]
            ) {
                indexes.add(i);
                i += 4;
            }
        }

        if (indexes.size() < 2)
            messages.add(message);
        else {
            for (int i = 0; i < indexes.size() - 1; ++i) {
                messages.add(Arrays.copyOfRange(message, indexes.get(i), indexes.get(i+1)+1));
            }
        }
    }

    /*
        Sometimes in one packet there are multiple messages.
        This method slits them and parses separately.
        Prints info about income message.
     */
    public void parse() {
        splitMessages();

        for (byte[] msg : messages) {
            byte[] magic = Arrays.copyOfRange(msg, 0,4);
            StringBuilder command = new StringBuilder();
            byte[] payloadLength = Arrays.copyOfRange(msg, 16, 20);
            byte[] checksum = Arrays.copyOfRange(msg, 20, 24);
            byte[] payload = Arrays.copyOfRange(msg, 24, msg.length);

            for (byte cmd : Arrays.copyOfRange(msg, 4, 16))
                if (cmd != 0x00)
                    command.append((char)cmd);

            System.out.println("\n\n***** RECEIVED MESSAGE *****");
            System.out.println("Magic: " + Integer.toHexString(0x1000000 * (magic[0] & 0xff) + 0x10000 * (magic[1] & 0xff) + 0x100 * (magic[2] & 0xff) + (magic[3] & 0xff)));
            System.out.println("Command: " + command.toString());
            System.out.println("Payload length: " + ByteBuffer.wrap(payloadLength).order(ByteOrder.LITTLE_ENDIAN).getInt());
            System.out.println("Checksum: " + Integer.toHexString(0x1000000 * (checksum[0] & 0xff) + 0x10000 * (checksum[1] & 0xff) + 0x100 * (checksum[2] & 0xff) + (checksum[3] & 0xff)));
            System.out.println();

            switch (command.toString()) {
                case "ping":
                    PingPayloadParser parser = new PingPayloadParser(payload);
                    parser.parse();
                    new Message("pong", new PongPayload(parser.getNonce()).getPayload());
                    break;

                case "addr":
                    GetAddrPayloadParser addrPayloadParser = new GetAddrPayloadParser(payload);
                    addresses = addrPayloadParser.parse(true);
                    break;

                case "headers":
                    HeadersToListParser headersPayloadParser = new HeadersToListParser(payload);
                    headers = headersPayloadParser.parse(true);
                    break;

                case "inv":
                    GetHeadersPayloadParser inventoryVectorParser = new GetHeadersPayloadParser(payload);
                    inventoryVectors = inventoryVectorParser.parse(true);
                    break;
            }

            System.out.println("****************************\n");
            System.out.print("BitLab > ");
        }
    }
}

/*
    This class parses "headers" message's payload, received after sending "getheaders".
 */
class GetHeadersPayloadParser {
    private byte[] payload;
    int count;

    public GetHeadersPayloadParser(byte[] payload) {
        this.payload = payload;
    }

    public ArrayList<InventoryVectorStruct> parse(boolean shouldPrint) {
        int length = Message.InvIntLength(payload[0]);
        count = Message.InvInt(payload);

        ArrayList<InventoryVectorStruct> list = new ArrayList<>();

        if (shouldPrint)
            System.out.println("Received " + count + " blocks:");

        for (int i = 0; i < count; ++i) {
            InventoryVectorParser inventoryVectorParser = new InventoryVectorParser(
                    Arrays.copyOfRange(payload, length + i * 36,  length + (i+1) * 36)
            );

            InventoryVectorStruct inventoryVectorStruct = inventoryVectorParser.parse();
            list.add(inventoryVectorStruct);

            if (shouldPrint) {
                System.out.println("---------------------------");
                System.out.println("Type: " + inventoryVectorStruct.typeToString());
                System.out.println("Services: " + inventoryVectorStruct.hashToString());
                System.out.println("---------------------------");
            }
        }

        return list;
    }
}

/*
    This class parses "inv" message's payload, received after sending "getblocks".
 */
class InventoryVectorParser {
    private byte[] payload;
    InventoryVectorStruct inventory = new InventoryVectorStruct();

    public InventoryVectorParser(byte[] payload) {
        this.payload = payload;
    }

    public InventoryVectorStruct parse() {
        this.inventory.type = Arrays.copyOfRange(payload, 0, 4);
        this.inventory.hash = Arrays.copyOfRange(payload, 4, 36);

        return this.inventory;
    }
}


class AddressParser {
    private AddressStruct addressStruct = new AddressStruct();
    private byte[] payload;

    public AddressParser(byte[] payload) {
        this.payload = payload;
    }

    public AddressStruct parse() {
        addressStruct.timestamp = Arrays.copyOfRange(payload, 0, 4);
        addressStruct.services = Arrays.copyOfRange(payload, 4, 12);
        addressStruct.address = Arrays.copyOfRange(payload, 12, 28);
        addressStruct.port = Arrays.copyOfRange(payload, 28, 30);

        return addressStruct;
    }
}

/*
    This class parses "address" message's payload, received after sending "getaddress".
 */
class GetAddrPayloadParser {
    private byte[] payload;
    int count;

    public GetAddrPayloadParser(byte[] payload) {
        this.payload = payload;
    }

    public ArrayList<AddressStruct> parse(boolean printInfo) {
        int length = Message.InvIntLength(payload[0]);
        count = Message.InvInt(payload);

        ArrayList<AddressStruct> list = new ArrayList<>();

        if (printInfo)
            System.out.println("Received " + count + " addresses:");

        for (int i = 0; i < count; ++i) {
            AddressParser addressParser = new AddressParser(
                Arrays.copyOfRange(payload, length + i * 30,  length + (i+1) * 30)
            );

            AddressStruct addressStruct = addressParser.parse();
            list.add(addressStruct);

            if (printInfo) {
                System.out.println("---------------------------");
                System.out.println("Timestamp: " + addressStruct.timeStampToString());
                System.out.println("Services: " + addressStruct.servicesToString());
                System.out.println("Address: " + addressStruct.addressToString());
                System.out.println("Port: " + addressStruct.portToString());
                System.out.println("---------------------------");
            }
        }

        return list;
    }
}

/*
    This class parses "headers" message's payload, received after sending "getheaders".
 */
class HeadersToListParser {
    private byte[] payload;
    int count;

    public HeadersToListParser(byte[] payload) {
        this.payload = payload;
    }

    public ArrayList<HeaderStruct> parse(boolean printInfo) {
        int length = Message.InvIntLength(payload[0]);
        count = Message.InvInt(payload);
        ArrayList<HeaderStruct> list = new ArrayList<>();

        if (printInfo)
            System.out.println("Received " + count + " headers:");

        for (int i = 0; i < count; ++i) {
            HeadersToStructParser headersParser = new HeadersToStructParser(
                    Arrays.copyOfRange(payload, length + i * 81,  length + (i+1) * 81)
            );

            HeaderStruct blockHeader = headersParser.parse();
            list.add(blockHeader);

            if (printInfo) {
                System.out.println("---------------------------");
                System.out.println("Version: " + blockHeader.versionToString());
                System.out.println("Prev block: " + blockHeader.prevBlockToString());
                System.out.println("Merkle Root: " + blockHeader.merkleRootToString());
                System.out.println("Timestamp: " + blockHeader.timeStampToString());
                System.out.println("Bits: " + blockHeader.bitesToString());
                System.out.println("Nonce: " + blockHeader.nonceToString());
                System.out.println("TXN_Count: " + blockHeader.txnCountToString());
                System.out.println("---------------------------");
            }
        }

        return list;
    }
}

class HeadersToStructParser {
    byte[] payload;
    private HeaderStruct header = new HeaderStruct();

    public HeadersToStructParser(byte[] payload) {
        this.payload = payload;
    }

    public HeaderStruct parse() {
        header.version = Arrays.copyOfRange(payload, 0, 4);
        header.prevBlock = Arrays.copyOfRange(payload, 4, 36);
        header.merkleRoot = Arrays.copyOfRange(payload, 36, 68);
        header.timestamp = Arrays.copyOfRange(payload, 68, 72);
        header.bits = Arrays.copyOfRange(payload, 72, 76);
        header.nonce = Arrays.copyOfRange(payload, 76, 80);
        header.txnCount = ByteBuffer.allocate(1).array();

        return header;
    }
}

class PingPayloadParser {
    private byte[] payload;

    public PingPayloadParser(byte[] payload) {
        this.payload = payload;
    }

    public void parse() {

    }

    public byte[] getNonce() {
        return payload;
    }
}