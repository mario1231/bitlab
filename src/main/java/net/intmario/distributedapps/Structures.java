package net.intmario.distributedapps;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class Structures {
}

class InventoryVectorStruct {
    public byte[] type = new byte[4];
    public byte[] hash = new byte[32];

    public String typeToString() {
        return Arrays.toString(type);
    }

    public String hashToString() {
        String ret = "";

        for (int i = 0; i < 32; ++i)
            ret += Integer.toHexString(hash[31 - i] & 0xff);

        return ret;
    }
}

class HeaderStruct {
    public byte[] version = new byte[4];
    public byte[] prevBlock = new byte[32];
    public byte[] merkleRoot = new byte[32];
    public byte[] timestamp = new byte[4];
    public byte[] bits = new byte[4];
    public byte[] nonce = new byte[4];
    public byte[] txnCount = new byte[1];

    public String versionToString() {
        return Arrays.toString(version);
    }

    public String prevBlockToString() {
        String ret = "";

        for (int i = 0; i < 32; ++i)
            ret += Integer.toHexString(prevBlock[31 - i] & 0xff);

        return ret;
    }

    public String merkleRootToString() {
        String ret = "";

        for (int i = 0; i < 32; ++i)
            ret += Integer.toHexString(merkleRoot[31 - i] & 0xff);

        return ret;
    }

    public String timeStampToString() {
        DateFormat simple = new SimpleDateFormat("dd MMM yyyy HH:mm:ss:SSS Z");
        Date result = new Date(
                (long)(ByteBuffer.wrap(timestamp).order(ByteOrder.LITTLE_ENDIAN).getInt()) * 1000
        );

        System.out.println(Integer.toHexString(ByteBuffer.wrap(timestamp).getInt()));

        return simple.format(result);
    }

    public String bitesToString() {
        String ret = "";

        for (int i = 0; i < 4; ++i)
            ret += Integer.toHexString(bits[3 - i] & 0xff);

        return ret;
    }

    public String nonceToString() {
        String ret = "";

        for (int i = 0; i < 4; ++i)
            ret += Integer.toHexString(nonce[3 - i] & 0xff);

        return ret;
    }

    public String txnCountToString() {
        return Byte.toString(txnCount[0]);
    }
}

class AddressStruct {
    public byte[] timestamp = new byte[4];
    public byte[] services = new byte[8];
    public byte[] address = new byte[16];
    public byte[] port = new byte[2];

    public String timeStampToString() {
        DateFormat simple = new SimpleDateFormat("dd MMM yyyy HH:mm:ss:SSS Z");
        Date result = new Date(
                (long)(ByteBuffer.wrap(timestamp).order(ByteOrder.LITTLE_ENDIAN).getInt()) * 1000
        );

        System.out.println(Integer.toHexString(ByteBuffer.wrap(timestamp).getInt()));

        return simple.format(result);
    }

    public String servicesToString() {
        String ret = "0x";

        for (int i = services.length - 1; i >= 0; --i)
            ret += Integer.toHexString(services[i] & 0xff);

        return ret;
    }

    public String addressToString() {
        String ret = "";

        if (
                (address[0] & 0xff) == (address[1] & 0xff) &&
                (address[1] & 0xff) == (address[2] & 0xff) &&
                (address[2] & 0xff) == (address[3] & 0xff) &&
                (address[3] & 0xff) == (address[4] & 0xff) &&
                (address[4] & 0xff) == (address[5] & 0xff) &&
                (address[5] & 0xff) == (address[6] & 0xff) &&
                (address[6] & 0xff) == (address[7] & 0xff) &&
                (address[7] & 0xff) == (address[8] & 0xff) &&
                (address[8] & 0xff) == (address[9] & 0xff) && (address[9] & 0xff) == 0x00
                && (address[10] & 0xff) == (address[11] & 0xff) && (address[11] & 0xff) == 0xff
        ) {
            ret += (int)(address[12] & 0xff);
            ret += ".";
            ret += (int)(address[13] & 0xff);
            ret += ".";
            ret += (int)(address[14] & 0xff);
            ret += ".";
            ret += (int)(address[15] & 0xff);
        } else {
            for (int i = 0; i < 16; i += 2) {
                ret += Integer.toHexString(address[i] & 0xff);
                ret += ":";
                ret += Integer.toHexString(address[i+1] & 0xff);
            }
        }
        return ret;
    }

    public String portToString() {
        return Integer.toString((port[0] & 0xff) * 0x100 + (port[1] & 0xff));
    }
}